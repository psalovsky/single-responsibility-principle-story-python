# Single Responsibility Principle Refactoring Story
**To read the story**: https://codingstories.io/stories/6139f4a2f2cd3d0031cd8f91/6192f05f7e944b001d795c16

**To code yourself**: https://gitlab.com/coding-stories1/single-responsibility-principle-story-python

**Estimated reading time**: 20 min

## Story Outline
Welcome to refactoring story about Single Responsibility Principle (SRP) for SOLID training course.
This story is about proper decomposition of classes and methods to fit Single Responsibility Principle.
In this story you will see example of web service application that is able to store and manage information
about persons and their contacts. Also you will see application evolution during refactoring 
and improvements steps.

### Contacts Book
Contacts Book is a REST web service which exposes personal data as JSON.
Project is built using FastAPI framework. 

Class [Person](/src/person.py) keeps persons 
information about name, birthday, living address and various contacts like phone and IM.
[Person](/src/person.py) is exposed as REST resource 
to store and query by personal data using person's email as a primary key.

##### Run application
You should to switch to your project directory and execute following command:
```
cd single-responsibility-principle-story-python

virtualenv venv
source venv/bin/activate

pip install -r requirements.txt
```
Then to run application
```
python src/person_controller.py
```


##### Store new contact
As you see application is listening on port 3000.
To add new contact you should POST request to:
```
http://localhost:3000/contact
```
With the following JSON in the body
```
{
    "firstName": "John",
    "lastName": "Doe",
    "birthday": "2000-01-01",
    "contacts" : [{ "type":"EMAIL", "contact":"jdoe@email.dog"}]
}
```

##### Query contact by email
You might query contact by issuing GET request like:
```
http://localhost:3000/contact?email=jdoe@email.dog
```
Which might response with following JSON:
```
{
    "firstName": "John",
    "lastName": "Doe",
    "birthday": "2000-01-01",
    "addresses": [],
    "phones": [],
    "contacts": [
        {
            "type": "EMAIL",
            "contact": "jdoe@email.dog"
        }
    ]
}
```
##### Application evolution and extensions planning
At the first glance application is able to fulfill it's goals. But it might be not well
suitable for further evolution and extensions because of many violations.
One of most annoying here is Single Responsibility Principle violation.
Improving only that aspect alone we might dramatically improve application structure
and "fix" other violations.
